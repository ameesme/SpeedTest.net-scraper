var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app     = express();
var open = require('open');

var loadingLog = "========================================================<br>Scraping process started!<br>";


var currentArticle = 91051294; // Swap this for the latest id you scraped.
var currentTime = 0;
var totalTries = 0;
var failedConnection = 0;
var failedArticle = 0;

// Settings
var downloadTotal = currentArticle + 4700098957;
var scrapeUrl = 'http://www.speedtest.net/my-result/'

app.get('/scrape', function(req, res){
	res.send('<title>Scraping: '+currentArticle+' of '+downloadTotal+'</title><meta http-equiv="refresh" content="5"><style>body{background-color:#000000;}*{text-shadow:0 0 10px #00ff00;color:#00ff00;font-family:monospace;font-size:20px;}table{margin-top:20px;}</style><body><p>Scrape server started...<br>Scraper...</p><table><tr><td width="250">Scraping article:</td><td>'+currentArticle+'</td></tr><tr><td>Total :</td><td>'+downloadTotal+' </td></tr></table><table><tr><td width="250">Running time:</td><td>'+currentTime+' seconds</td></tr><tr><td>Remaining time:</td><td>'+Math.round((downloadTotal - currentTime * (currentArticle/currentTime)) / (currentArticle/currentTime))+' seconds</td></tr></table><table><tr><td width="250">Total connections:</td><td>'+totalTries+'</td></tr><tr><td width="250">Failed connections:</td><td>'+failedConnection+'</td></tr><tr><td>Failed :</td><td>'+failedArticle+' </td></tr></table><p>========================================================</p><p>'+loadingLog+'</p></body>');
})
if(!fs.existsSync('output.json')){
	fs.writeFile('output.json','[');
}
var timeRunner = setInterval(function(){
	currentTime++;
},1000);
scrapeArticle();

function pad(num, size) {
    var s = "0000000000" + num;
    return s.substr(s.length-size);
}

function scrapeArticle(){	
	if (currentArticle < (downloadTotal+1)) {
		totalTries++;
		url = scrapeUrl+pad(currentArticle,10);
		request(url, function(error, response, html){
			if(!error){
				var $ = cheerio.load(html);
				if (response.statusCode === 200 && $('.share-speed p').html()) {
					var jsonData = {'id': currentArticle, 
									'url' : response.request.req.path, 
									'statusCode': response.statusCode, 
									'downloadSpeed': $('.share-download p').html().replace('<span>Mb/s</span>',''),
									'uploadSpeed': $('.share-upload p').html().replace('<span>Mb/s</span>',''),
									'ping': $('.share-ping p').html().replace('<span>ms</span>',''),
									'isp': $('.share-isp p').html(),
									'server': $('.share-server p').html(),
									'date': Date.parse($('.share-meta-date').html().replace('at',''))
								};
					console.log('[SUCCESS] Scraped article '+currentArticle);
					loadingLog = '[SUCCESS]   Scraped article '+currentArticle+'<br>'+loadingLog;
				}else if(response.statusCode === 404) {
					var jsonData = {'id': currentArticle, 'url' : response.request.req.path, 'statusCode': response.statusCode};
					console.log('[404]     Got 404 for article '+currentArticle);
					loadingLog = '[404]     Got 404 for article '+currentArticle+'<br>'+loadingLog;
					failedArticle++;
				}else{
					var jsonData = {'id': currentArticle, 'url' : response.request.req.path, 'statusCode': response.statusCode};
					console.log('['+response.statusCode+']     Got '+response.statusCode+' for article '+currentArticle);
					loadingLog = '['+response.statusCode+']     Got '+response.statusCode+' for article '+currentArticle+'<br>'+loadingLog;
					failedArticle++;
				};
				fs.appendFileSync('output.json', JSON.stringify(jsonData)+',\n');
				currentArticle++;
				scrapeArticle();
			}else if(error){
				loadingLog = '[ERROR]   Could not establish connection or got Timeout.<br>Trying again...<br>'+loadingLog;
				console.log('Error occurred.');
				console.log(error);
				failedConnection++;
				scrapeArticle();
			}else{
				loadingLog = '[ERROR]   Unknown error<br>Trying again...<br>'+loadingLog;
				console.log('Unknown error');
				failedConnection++;
				scrapeArticle();
			}
		})
	}else{
		clearInterval(timeRunner);
		fs.appendFile('output.json', ']', function(err){});
		loadingLog = 'Scraped '+ (currentArticle - 1) +'  in '+currentTime+' seconds.<br>STOPPED SCRAPING!<br>========================================================<br>FINISHING DATA WRITE.<br>'+loadingLog;
	};
};
// open('http://localhost:8081/scrape');
app.listen('8081',function(){
	console.log('================================= STARTING SCRAPE SERVER =================================');
	console.log('Check progress at http://localhost:8081/.');
})
exports = module.exports = app; 