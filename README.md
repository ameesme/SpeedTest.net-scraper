Speedtest.net-scraper
===========================
## Questions
### Is this even legal?
I don't know.

### This code sucks.
Yes.

### Ever heard of threads?
Yes.

### Should I use this?
Probably not.

### What data do I get when I run this?
You'll get the following information for each speedtest in a little (or pretty huge) JSON-file.
- Download speed
- Upload speed
- Ping time
- ISP
- Server location
- Date
- Result URL

### How long will it take to scrape their site?
I don't know. Probably a ~~week~~ year or two.

### Any other advice?
Yes.
- Run this on a VPS or Raspberry Pi you have lying around
- Use a VPN that switches IP every 10 minutes or so

### How do I resume the scraping after it closed?
- Check the latest ID you scraped in your output.json
- Replace currentArticle in app.js:11 with that id.
- Be done with it.
